﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data.Fields;

namespace ItZynergyAps.Sitecore650CodeSamples.Fields
{
    public class TestFieldType
    {

        public static void TestFieldTypeWithFieldManager(string fieldName)
        {
            Field field = Sitecore.Context.Item.Fields[fieldName];

            if (FieldTypeManager.GetField(field) is TextField)
            {
                // Do something
            }

            if (FieldTypeManager.GetField(field) is HtmlField)
            {
                // Do something
            }

            if (FieldTypeManager.GetField(field) is LookupField)
            {
                // Do something
            }

            if (FieldTypeManager.GetField(field) is MultilistField)
            {
                // Do something
            }
        }
    }
}
